module quadandornot(
  input a1, a2, 
  input b1, b2,
  input c1, c2,
  input d1, d2,
  output e
);

  assign e = ~(
    (a1 & a2) |
    (b1 & b2) |
    (c1 & c2) |
    (d1 & d2) 
  );

endmodule

module triandornot(
  input a1, a2, 
  input b1, b2,
  input c1, c2,
  output e
);

  assign e = ~(
    (a1 & a2) |
    (b1 & b2) |
    (c1 & c2)
  );

endmodule

// Full adder
module fulladder(
  input a,
  input b,
  input C,
  output S,
  output Sn,
  output Cn
);

  logic q2out;
  logic q1out;
  
  quadandornot q1(C, a &b, a, q2out, b, q2out, C, q2out, q1out);
  triandornot q2(a, b, a, C, b, C, q2out);
  
  assign S = ~q1out;
  assign Sn = q1out;
  assign Cn = q2out;

endmodule
