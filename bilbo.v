module d_latch(
  input d, e,
  output q, nq
);

  logic s, r;

  nor g1(q, r, nq);
  nor g2(nq, s, q);
  and g3(r, e, nd);
  and g4(s, e, d);
  not g5(nd, d);

endmodule

module bixor(
  	input a,b,c,d,
  	output out
);
	assign out = a ^ b ^ c ^ d;  
endmodule

module andornot(
  	input a1, a2,
  	input b1, b2,
    output out
);
  assign out = (a1 & a2) ~| (b1 & b2 );
endmodule

// Write your modules here!
module bilbo(
	input b1, b2,
    input ds,
    input [3:0] din,
  output [3:0] dout,
);
  logic d1d, d2d, d3d, d4d;
  logic d1q, d2q, d4q, d4q;
  logic b1n;
  logic b2n;
  logic tmp;
  logic aon_out;
  logic bx_out;
  
  bigxor bx(d1q, d2q, d3q, d4q, bx_out);
  andornot aon(b1, ds, b1n, bx_out, aon_out);
  
  d_latch d1(d1d, clk, d1q, dout[0]);
  
  assign tmp = b2 | b1n;
  assign d1d = (din[0]|&b2n)^(tmp|&aon_out);
  assign b1n = ~b1;
  assign b2n = ~b2;
  
endmodule

