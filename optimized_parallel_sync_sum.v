// Half adder
module quadandornot(
  input a1, a2, 
  input b1, b2,
  input c1, c2,
  input d1, d2,
  output e
);

  assign e = ~(
    (a1 & a2) |
    (b1 & b2) |
    (c1 & c2) |
    (d1 & d2) 
  );

endmodule

module triandornot(
  input a1, a2, 
  input b1, b2,
  input c1, c2,
  output e
);

  assign e = ~(
    (a1 & a2) |
    (b1 & b2) |
    (c1 & c2)
  );

endmodule

// Full adder
module fulladder(
  input a,
  input b,
  input C,
  output S,
  output Sn,
  output Cn
);

  logic q2out;
  logic q1out;
  
  quadandornot q1(C, a &b, a, q2out, b, q2out, C, q2out, q1out);
  triandornot q2(a, b, a, C, b, C, q2out);
  
  assign S = ~q1out;
  assign Sn = q1out;
  assign Cn = q2out;

endmodule

module optimized_pararel_sum(
	input a1, b1, C,
  	input a2, b2,
    input a3, b3,
  	input a4, b4,
  output S1, S2, S3, S4, C4
);
  logic C1, C2, C3;
  logic S1n, S2n, S3n, S4n;
  fulladder fa1(a1, b1, C, S1, S1n,C1);
  fulladder fa2(~a2, ~b2, C1, S2n, S2, C2);
  fulladder fa3(a3, b3, C2, S3, S3n, C3);
  fulladder fa4(~a4, ~b4, C3, S4n, S4, C4);
  
endmodule
