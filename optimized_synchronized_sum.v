module Dflipflop(
  input D,
  input clk,
  input Srn,
  input Rrn,
  output Q,
  output Qn
);
  logic Sn, Rn;
  assign Q = ~(Srn & Sn & Qn);
  assign Qn = ~(Rn & Rrn & Q);
  assign Sn = D ~& clk;
  assign Rn = Sn ~& clk;
endmodule

module quatornot(
	input a, b, c, d,
    output e,
);
  assign e = ~(a | b | c | d);
endmodule

module triornot(
	input a, b, c,
    output d,
);
  assign d = ~(a | b | c);
endmodule

// Full adder
module fulladder(
  input a,
  input b,
  input C_prev,
  input SET_C,
  input RESET_C,
  input clk,
  output S,
  output C
);
  
  logic tnoout, Q;

  quatornot qno( C_prev & b & a, a & C , b & C, C_prev & C , S);
  triornot tno(a & b, a & C_prev, b & C_prev, tnoout);
  Dflipflop dt(tnoout,clk, SET_C, RESET_C, q, C );
  
endmodule

