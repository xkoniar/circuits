module andornot(
	input a1, a2,
  	input b1, b2, b3,
  output c
);
  assign c = (a1 & a2) ~| (b1 & b2 & b3);
endmodule

module asyncsum(
  input a,
  input b,
  input C_prev,
  input N_prev,
  input V,
  output C,
  output N,
  output s,
  output k
);

  logic P, Ptemp;
  logic G;
  
  assign Ptemp = a ~& b;
  assign P = (a ~& Ptemp) ~& (Ptemp ~& b);
  assign G = ~Ptemp;
  
  andornot aon1(G, V, P, V , C_prev, N);
  andornot aon2(a~|b, V, P,  N_prev, V, C);

  assign s = P ^ C_prev;
  assign k = N ~& C;
  
endmodule

