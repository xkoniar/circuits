# Materialy pro vyuku obvodu

Repozitar obsahuje zdrojove soubory pro webovy nastroj <http://digitaljs.tilk.eu/>.
Nastroj zkompiluje zdrojovy kod ve verilogu a vytvori vizualizaci daneho obvodu.
Vizulizace se da ovladat a simulovat jeji prubeh.

Soucastky obvodu jsou po kompilaci casto neprijemne rozmisteny. Misto verilogu proto jsou dodany i definice primo pro tuto aplikaci, ulozene v .json souborech.

## Mapa souboru

1. Optimalizovana jednoradova binarni scitacka
    * optimized_oneline_sum.v
    * optimized_oneline_sum.json
2. Optimalizovana synchronni seriova binarni scitacka
    * optimized_synchronized_sum.v
    * optimized_synchronized_sum.json
3. Optimalizovana synchronni paralelni binarni scitacka
    * optimized_parallel_sync_sum.v
    * optimized_parallel_sync_sum.json
4. Zapojeni jednoho bitu kvazi-asynchronne rizene scitacky
    * quazi_async_sum.v
    * quazi_async_sum.json 
5. Schema paralelniho generatoru parity s kontrolnimi obvody
    * pararell_parity.v
    * pararell_parity.json
6. Kontrolni obvod obousmerneho binarniho citace (predikce parity)
    * control_bin_sum.v
    * control_bin_sum.json
7. LFSR
    * je soucasti ukazek od autora
8. BILBO
    * bilbo.v
    * bilbo.json

## Navod

Zakladni ovladaci prvky aplikace jsou vrchni panel a vstup pro zdrojovy kod.

![](pics/panel.png)

Vrchni panel obsahuje tri skupiny prvku:

1. Ovladani casu aplikace, obsahuje dulezite prvky: pozastaveni (pause), spusteni simulace (play) a krokovani simulace (arrow)
2. Hodiny
3. Panel na ukladani simulace, v tomto poradi: nacist ze souboru, ulozit do souboru, vytvorit odkaz
    * "Nacist ze souboru" pouzivejte na nacteni .json souboru z tohoto repozitare

![](pics/input.png)

Vstup je jednoduchy editor pro verilog, upozornuji ze se jedna o webovou stranku, takze pokud ji nahodou zavrete, ztracite vse co jste napsali.
Nad vstupem pro zdrojovy kod je mozne si vybrat nekolik ukazek od autora aplikace.


![](pics/circuit.png)

Samotny obvod se vykresli v hlavnim okne, cervena je pro LOW hodnotu, zelena pro HIGH.

1. **d** a **e** jsou vstupy, jsou ovladatelny mysi.
2. **nq** a **q** jsou vystupy.
3. Aplikace pouziva symboly dle <https://en.wikipedia.org/wiki/Electronic_symbol>

![](pics/tools.png)

Po najeti mysi na jednotlive spoje v obvodu, je mozne pouzit dalsi nastroje:

1. Krizek slouzi k zruseni spojeni (je mozne ho znovu vytvorit).
2. Lupa zapne sledovani signalu v analyzatoru.

![](pics/analyzator.png)

Analyzator v realnem case sleduje vybranne logicke signaly a umoznuje tak sledovat zmeny, vcetne zpozdeni mezi signaly.

