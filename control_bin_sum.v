module d_latch(
  input d, e,
  output q, nq
);

  logic s, r;

  nor g1(q, r, nq);
  nor g2(nq, s, q);
  and g3(r, e, nd);
  and g4(s, e, d);
  not g5(nd, d);

endmodule

module quadandnot(
	input a1, a2, a3, a4,
  	output out
);
  assign out = ~(a1 & a2 & a3 & a4);
endmodule


module triandnot(
	input a1, a2, a3,
  	output out
);
  assign out = ~(a1 & a2 & a3);
endmodule

module sumcontrol(
  input y1, y2, y3, y4,
  input dir, clk,
  output error
);
  logic y1n, y2n, y3n;
  logic tmp1, tmp2, tan1out, tan2out, dirn, Q, Qn;
  
  assign y1n = ~y1;
  assign y2n = ~y2;
  assign y3n = ~y3;
  assign dirn = ~dir;
  assign tmp1 = (y1 ~^ y2) ~^ (y3 ~^ y4);
  assign error = Q ~^ tmp1;
  
  triandnot tan1(y2, dir, y3n, tan1out);
  triandnot tan2(y2n, dirn, y3, tan2out);
  quadandnot qan(y1n ~& dir, tan1out, y1 ~& dirn, tan2out, tmp2);
  d_latch d(tmp1~^tmp2, clk, Q, Qn);
  
endmodule
