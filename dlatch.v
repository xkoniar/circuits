module Dflipflop(
  input D,
  input clk,
  input Srn,
  input Rrn,
  output Q,
  output Qn
);
  logic Sn, Rn;
  assign Q = ~(Srn & Sn & Qn);
  assign Qn = ~(Rn & Rrn & Q);
  assign Sn = D ~& clk;
  assign Rn = Sn ~& clk;
endmodule
