module andor(
	input a1, a2,
  	input b1, b2,
    output c, cn
);
  assign cn = (a1 & a2) ~| (b1 & b2);
  assign c = ~cn;
endmodule


module pararel_parity(
  input a0, 
  input a1,
  input a2,
  input a3,
  input a4,
  input a5,
  input a6,
  input a7,
  input ap,
  output Peven, Podd,
  output error, ok, gen_error
);
  logic ao1out, ao1outn;
  logic ao2out, ao2outn;
  logic ao3out, ao3outn;
  logic ao4out, ao4outn;
  logic ao5out, ao5outn;
  logic ao6out, ao6outn;
  logic ao7out, ao7outn;
  
  andor ao1(a0, a1, ~a0, ~a1, ao1out, ao1outn);
  andor ao2(a2, a3, ~a2, ~a3, ao2out, ao2outn);
  andor ao3(a4, a5, ~a4, ~a5, ao3out, ao3outn);
  andor ao4(a6, a7, ~a6, ~a7, ao4out, ao4outn);
  
  andor ao5(ao1out, ao2out, ao1outn, ao2outn, ao5out, ao5outn);
  andor ao6(ao3out, ao4out, ao3outn, ao4outn, ao6out, ao6outn);
 
  andor ao7(ao5out, ao6out, ao5outn, ao6outn, ao7out, ao7outn);
  
  andor f(ao7out, ap, ao7outn, ~ap, error, ok);
  
  assign gen_error = ao7out ~^ ao7outn;
  assign Peven = ao7outn;
  assign Podd = ao7out;
endmodule
